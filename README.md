# Fuzzy Logic

Fuzzy Logic approach used to predict the levels of aeration, agitation and feeding rate basing on the O2 concentration, turbidity, concentration of NaCl, temperature and pH.
Includes the graphical output of the membership functions, rules and output values.